import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:food_app/widgets/custom_text_field.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  XFile? imageXFile;
  final TextEditingController locationController = TextEditingController();
  final ImagePicker _picker = ImagePicker();
  final _formKey = GlobalKey<FormState>();
  PermissionStatus _permissionStatus = PermissionStatus.denied;

  String? name;
  String? email;
  String? password;
  String? confirmedPassword;
  String? phone;
  String? location;
  Position? position;
  List<Placemark>? placeMarks;

  Future<void> _getImage() async {
    PickedFile? image = await _picker.getImage(source: ImageSource.gallery);
    imageXFile = XFile(image?.path ?? '');
    setState(() {
      imageXFile;
    });
  }

  void getCurrentLocation() async {
    if (await Permission.locationWhenInUse.isGranted) {
      Position newPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );
      position = newPosition;
      placeMarks = await placemarkFromCoordinates(
        newPosition.latitude,
        newPosition.longitude,
      );
      Placemark pMark = placeMarks![0];
      String completeAddress =
          '${pMark.subThoroughfare} ${pMark.thoroughfare}, ${pMark.subLocality} ${pMark.locality}, ${pMark.subAdministrativeArea}, ${pMark.administrativeArea}, ${pMark.postalCode}, ${pMark.country}';
      locationController.text = completeAddress;
    } else {
      requestPermission();
    }
  }

  Future<void> requestPermission() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled) {
      final status = await Permission.locationWhenInUse.request();
      setState(() {
        print("status: $status");
        _permissionStatus = status;
        print(_permissionStatus);
      });
    }
  }

  void _listenForPermissionStatus() async {
    final status = await Permission.locationWhenInUse.status;
    setState(() => _permissionStatus = status);
  }

  void checkServiceStatus(BuildContext context) async {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content:
          Text((await Permission.locationWhenInUse.serviceStatus).toString()),
    ));
  }

  @override
  void initState() {
    super.initState();
    _listenForPermissionStatus();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          const SizedBox(height: 10),
          InkWell(
            child: CircleAvatar(
              radius: MediaQuery.of(context).size.width * 0.20,
              backgroundColor: Colors.white,
              backgroundImage:
                  imageXFile != null ? FileImage(File(imageXFile!.path)) : null,
              child: imageXFile == null
                  ? Icon(
                      Icons.add_photo_alternate,
                      color: Colors.grey,
                      size: MediaQuery.of(context).size.width * 0.20,
                    )
                  : null,
            ),
            onTap: () {
              _getImage();
            },
          ),
          const SizedBox(height: 10),
          Form(
            key: _formKey,
            onWillPop: () async {
              final isValid = _formKey.currentState?.validate();
              return isValid ?? false;
            },
            child: Center(
              child: Column(
                children: [
                  CustomTextFormField(
                    icon: Icons.person,
                    hintText: 'Name',
                    onSaved: (value) {
                      name = value;
                    },
                  ),
                  CustomTextFormField(
                    icon: Icons.email,
                    hintText: 'Email',
                    onSaved: (value) {
                      email = value;
                    },
                    validator: (value) {
                      if (value != null && EmailValidator.validate(value)) {
                        return null;
                      }
                      return 'Invalid email';
                    },
                  ),
                  CustomTextFormField(
                    icon: Icons.lock,
                    hintText: 'Password',
                    isObscure: true,
                    onSaved: (value) {
                      password = value;
                    },
                  ),
                  CustomTextFormField(
                    icon: Icons.lock,
                    hintText: 'Confirm Password',
                    isObscure: true,
                    onSaved: (value) {
                      confirmedPassword = value;
                    },
                  ),
                  CustomTextFormField(
                    icon: Icons.phone,
                    hintText: 'Phone',
                    onSaved: (value) {
                      phone = value;
                    },
                  ),
                  CustomTextFormField(
                    controller: locationController,
                    icon: Icons.my_location,
                    hintText: 'Cafe/Restaurant Address',
                    isEnabled: false,
                    onSaved: (value) {
                      location = value;
                    },
                  ),
                  const SizedBox(height: 20),
                  Container(
                    width: 400,
                    height: 55,
                    alignment: Alignment.center,
                    child: ElevatedButton.icon(
                      label: const Text(
                        'Set my current Location',
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.amber,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                      icon: const Icon(
                        Icons.location_on,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        if (Platform.isIOS) {
                          getCurrentLocation();
                        }
                      },
                    ),
                  ),
                  const SizedBox(height: 5),
                  ElevatedButton(
                    child: const Text(
                      'Sign up',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.cyan,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 80,
                        vertical: 10,
                      ),
                    ),
                    onPressed: () {
                      if (_formKey.currentState?.validate() == true) {
                        _formKey.currentState?.save();
                        print('valid');
                        print('name $name');
                        print('email $email');
                        print('password $password');
                        print('confirmedPassword $confirmedPassword');
                        print('phone $phone');
                        print('location $location');
                      }
                      checkServiceStatus(context);
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

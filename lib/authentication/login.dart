import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:food_app/widgets/custom_text_field.dart';

class LoginScreen extends StatefulWidget {
  final _formKey = GlobalKey<FormState>();

  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Image.asset(
                'images/seller.png',
                height: 270,
              ),
            ),
          ),
          Form(
            key: widget._formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                CustomTextFormField(
                  icon: Icons.email,
                  hintText: 'Email',
                  isObscure: false,
                  validator: (value) {
                    if (value == null || !EmailValidator.validate(value)) {
                      return "Please enter a valid email";
                    }
                  },
                ),
                CustomTextFormField(
                  icon: Icons.lock,
                  hintText: 'Password',
                  isObscure: true,
                  validator: (value) {
                    if (value != null) {
                      const exp =
                          "^(?=.{8,32}\$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%^&*(),.?:{}|<>]).*";
                      bool passValid = RegExp(exp).hasMatch(value);
                      if (!passValid) {
                        return 'Min 8 chars, uppercase & lowercase, number & special char';
                      }
                    }
                  },
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: ElevatedButton(
                    onPressed: () {
                      if (widget._formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Processing Data')),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Processing Data')),
                        );
                      }
                    },
                    child: const Text('Login'),
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(45),
                    ),
                  ),
                ),
              ],
            ),
            onChanged: () {
              print('onChanged');
            },
            onWillPop: () async {
              print('onChanged');
              return true;
            },
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final IconData? icon;
  final String? hintText;
  final String? labelText;
  final String? initialValue;
  final bool isObscure;
  final bool isEnabled;
  final String? Function(String?)? validator;
  final String? Function(String?)? onSaved;
  final TextEditingController? controller;

  const CustomTextFormField({
    Key? key,
    this.icon,
    this.hintText,
    this.labelText,
    this.initialValue,
    this.isObscure = false,
    this.isEnabled = true,
    this.validator,
    this.onSaved,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(10),
      child: TextFormField(
        initialValue: initialValue,
        validator: validator,
        onSaved: onSaved,
        obscureText: isObscure,
        enabled: isEnabled,
        cursorColor: Theme.of(context).primaryColor,
        controller: controller,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
          iconColor: Colors.white,
          fillColor: Theme.of(context).primaryColor,
          border: InputBorder.none,
          prefixIcon: Icon(
            icon,
            color: Colors.cyan,
          ),
        ),
      ),
    );
  }
}
